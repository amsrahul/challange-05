// Siapkan routing express kamu disini...
// jangan lupa listen di port sesuai process.env ya, defaultnya bebas misal 8000
const { PORT = 8080} = process.env
const controllers = require('./controllers');
const express = require('express');
const multer = require("multer");
const form = multer({dest: 'public/img'})
const app = express();

app.set('view engine', 'ejs');
app.use(express.json());
app.use('/assets', express.static('public'));

app.get('/', controllers.list);
app.get('/form', controllers.form);
app.get('/form/:id', controllers.form);
app.get("/api/v1/cars", controllers.carGetAll);
app.get("/api/v1/cars/:id", controllers.carGetId);

app.post("/api/v1/cars", controllers.carPost);
app.post("/api/v1/car_upload", form.single("photo"), controllers.carUpload);

app.put('/api/v1/cars/:id', controllers.carPut);

app.delete("/api/v1/cars/:id", controllers.carDelete);

app.listen(PORT, () => {
    console.log(`server running on port: ${PORT} || url : http://localhost:${PORT}`)
})