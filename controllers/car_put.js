const { Cars } = require("../models");

async function carPut(req, res) {
    await Cars.update({
        name: req.body.name,
        price: req.body.price,
        size_id: req.body.size_id,
        photo: req.body.photo,
    }, {
        where: {
            id: req.body.id
        }
    });

    res.send(`Data berhasil diupdate `);
}

module.exports = carPut;