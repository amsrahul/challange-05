// exports semua controller disini ...
module.exports = {
    list: require('./list'),
    form: require('./form'),
    carPost: require('./car_post.js'),
    carUpload: require('./car_upload'),
    carGetAll: require('./car_get_all'),
    carDelete: require('./car_delete'),
    carPut: require('./car_put'),
    carGetId: require('./car_get_id')
}