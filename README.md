# Car Management Dashboard
description merupakan sebuah aplikasi web yang digunakan untuk manajemen data mobil seperti lihat, tambah, edit, dan delete data (crud) 

## How to run
guide <br>

ubah username,password,database,host,dialect in development at config.json setelah menjalankan sequelize <br>

yarn start untuk menjalankan. setelah itu pada anda akan mendapatkan alamat url untuk mengakses car management dashboard

```bash
yarn install
yarn sequelize-cli init
yarn sequelize-cli db:create
yarn sequelize-cli model:generate --name Sizes --attributes name:string --underscored
yarn sequelize-cli model:generate --name Cars --attributes name:string,price:integer,size_id:integer,photo:string --underscored
yarn migrate
```

## Endpoints
list endpoints 

GET("/api/v1/cars") : untuk mendapatkan semua data mobil <br>
GET("/api/v1/cars/:id") : untuk mendapatkan data mobil berdasarkan id <br>
POST("/api/v1/cars") : untuk menampilkan data mobil <br>
POST("/api/v1/car_upload") : untuk menampilkan data mobil melalui form <br>
PUT("/api/v1/cars/:id") : untuk memperbarui data mobil <br>
DELETE("/api/v1/cars/:id") : untuk menghapus data mobil


## Directory Structure

```
.
├── config
│   └── config.json
├── controllers
├── migrations
├── models
│   └── index.js
├── public
│   ├── css
│   ├── fonts
│   ├── img
│   ├── js
│   └── uploads
├── seeders
├── views
│   ├── templates
│   │   ├── footer.ejs
│   │   └── header.ejs
│   ├── form.ejs
│   └── list.ejs
├── .gitignore
├── README.md
├── index.js
├── package.json
└── yarn.lock
```

## ERD
![Entity Relationship Diagram](public/img/erd.png)
